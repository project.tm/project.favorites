<?

use Bitrix\Main\Application;

define("FAVORIT_AJAX", true);
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

switch ($_REQUEST["TEMPLATE_NAME"]) {
    case 'shop':
        define('SITE_ID', 's2');
        break;
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
$request = Application::getInstance()->getContext()->getRequest();
$arResult['isOk'] = $APPLICATION->IncludeComponent("portal:favorites.ajax.list", $request->get('TEMPLATE_NAME'), Array(
    "IS_AJAX" => 'Y',
    "DELETE" => true,
    'FILTER' => $request->get('FILTER'),
    "PAGEN" => $request->get("PAGEN_1"),
    "TYPE" => $request->get("TYPE"),
    "ITEM" => $request->get("ITEM"),
    "ELEMENT_ID" => (int) $request->get("ELEMENT_ID"),
        ));
$arResult['content'] = ob_get_clean();
echo json_encode($arResult);
