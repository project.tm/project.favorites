<?php

use Bitrix\Main\Loader,
    Igromafia\Game\Model\FavoritTable,
    Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class PortalFavoritesList extends CBitrixComponent {

    public function executeComponent() {
        if (CUser::IsAuthorized() and Loader::includeModule('igromafia.game')) {

            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';

            $userId = $GLOBALS['USER']->GetID();
            if (!empty($this->arParams['DELETE']) and ! empty($this->arParams['ELEMENT_ID'])) {
                $rsData = FavoritTable::getList(array(
                            'select' => array('ID'),
                            'filter' => array(
                                'UF_ENTITY' => $this->arParams['ITEM'],
                                'UF_USER' => $userId,
                                'UF_ID' => $this->arParams['ELEMENT_ID']
                            ),
                ));
                $rsData = new CDBResult($rsData);
                $this->arResult['INFAVORITES'] = $rsData->Fetch();
                if ($this->arResult['INFAVORITES']) {
                    FavoritTable::Delete($this->arResult['INFAVORITES']['ID']);
                }
            }

            $this->arResult['FILTER'] = 'favoritFllter';
            switch ($this->arParams['TYPE']) {
                case 'GAME':
                    $this->arParams['FILTER_ENTITY_ALL'] = $this->arParams['FILTER_ENTITY'] = array(
                        'LAVKADJOITEM',
                        'GAME',
                        'RELEASE',
                        'NEWS',
                        'ARTICLES',
                        'REVIEWS',
                    );
                    if(in_array($this->arParams['FILTER'], $this->arParams['FILTER_ENTITY'])) {
                        $this->arParams['FILTER_ENTITY'] = array(
                            $this->arParams['FILTER']
                        );
                    }
                    break;

                default:
                    $this->arParams['FILTER_ENTITY'] = $this->arParams['TYPE'];
                    break;
            }

            $GLOBALS[$this->arResult['FILTER']] = array(
                'UF_ENTITY' => $this->arParams['FILTER_ENTITY'],
                'UF_USER' => $userId,
            );

            $this->arResult['PAGEN'] = empty($_GET['PAGEN_1']) ? 1 : $_GET['PAGEN_1'];
            $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
            $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
            $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
            $this->includeComponentTemplate();
            return true;
        }
    }

}
